// this is the id of the form
$("#frmLogin").on("submit", function (e) {
    var url = contexto + "usuarios";
    e.preventDefault(); // avoid to execute the actual submit of the form.

    var datos = $('#frmLogin').serialize();

    $.ajax({
        type: "GET",
        url: url,
        dataType: 'json',
        contentType: 'application/json',
        mimeType: 'application/json',
        data: datos, // serializes the form's elements.
        success: function (data) {
            console.log(JSON.stringify(data)); // show response from the php script.
            if (data.usuario === null) {
                alert("Revise su informacion de inicio");
            } else {
                window.location.replace( "sipcp/index.html");
            }

        }
    });

});
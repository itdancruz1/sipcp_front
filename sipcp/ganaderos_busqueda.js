
$(document).ready(function () {
    var url = contexto + "personas";
    $.ajax({

        url: url,
        method: 'GET',
        dataType: 'json',
        success: function (data) {

        

            $('#tblGanaderos').DataTable({
                "paging": true,
                "lengthChange": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true, "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
                data: data,
                columns: [

                    { "data": "idPersona" },
                    { "data": "nombre" },
                    { "data": "apellidos" },
                    { "data": "rfc" },
                    { "data": "direccion" },
                    { "data": "permisoMatar" },
                    { "data": "idPersona",
                    render: function ( dato, type, row ) {
                        return '<a href="ganaderos_captura.html?id='+ dato + '">Revisar </a>';
                    }}
                ]
            }).buttons().container().appendTo('#tblGanaderos_wrapper .col-md-6:eq(0)');
        }
    });


});
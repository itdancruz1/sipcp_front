
$("#btnGuardar").on("click", function (e) {

    e.preventDefault(); // avoid to execute the actual submit of the form.
    $('#tblGanaderos').DataTable().destroy();


    var url = contexto + "patentes/reportes?fi=" + $("#fechaInicioV").val() + "&ff=" + $("#fechaFinV").val();
    $.ajax({

        url: url,
        method: 'GET',
        dataType: 'json',
        success: function (data) {

            $('#tblGanaderos').DataTable({
                "paging": true,
                "lengthChange": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true, "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
                data: data,
               
            }).buttons().container().appendTo('#tblGanaderos_wrapper .col-md-6:eq(0)');
        }
    });








});

$(function () {
    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('yyyy/mm/dd', { 'placeholder': 'yyyy/mm/dd' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#fechaInicio').datetimepicker({
        format: 'YYYY/MM/DD'
    });
    $('#fechaFin').datetimepicker({
        format: 'YYYY/MM/DD'
    });


})
// this is the id of the form
$("#frmPersona").on("submit", function (e) {
    var url = contexto + "predios";
    e.preventDefault(); // avoid to execute the actual submit of the form.

    var datos = $('#frmPersona').serializeJSON();


    $.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
        contentType: 'application/json',
        mimeType: 'application/json',
        data: JSON.stringify(datos), // serializes the form's elements.
        success: function (data) {
            console.log(JSON.stringify(data)); // show response from the php script.
            alert("Guardado correcto");

        }
    });

});


var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};


$(document).ready(function () {
    var urlBlob = contexto + "/predios";
    var id = getUrlParameter('id');

    if (undefined !== id) {

        $.ajax({
            url: urlBlob + "/" + id,
            method: 'GET',
            type: 'GET', // For jQuery < 1.9
            success: function (persona) {
                //alert("datos recuperados correctamente");
                console.log(persona);
                $("#idPredio").val(id);
                $("#predio").val(persona.predio);
                $("#direccion").val(persona.direccion);
            }
        });
    }

});
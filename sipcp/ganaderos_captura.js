// this is the id of the form
$("#frmPersona").on("submit", function (e) {
    var url = contexto + "personas";
    e.preventDefault(); // avoid to execute the actual submit of the form.

    var permiso = $('#permiso').is(":checked");
    if (permiso) {
        $('#permisoMatar').val(1);
    } else {
        $('#permisoMatar').val(0);
    }
    var datos = $('#frmPersona').serializeJSON();
    datos = JSON.stringify(datos);
    //console.log(datos);    

    $.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
        contentType: 'application/json',
        mimeType: 'application/json',
        data: datos, // serializes the form's elements.
        success: function (data) {
            console.log(JSON.stringify(data)); // show response from the php script.
            console.log("ID " + data.respuesta.idPersona);
            $('#idFoto').val(data.respuesta.idPersona);
            $('#frmFoto').submit();

        }
    });

});

$('#frmFoto').on("submit", function (event) {
    var urlBlob = contexto + "personas/blob";
    event.preventDefault();
    var dataFoto = new FormData();
    $.each(jQuery('#frmFoto input[name=imageFile]')[0].files, function (i, file) {
        dataFoto.append('foto', file);
    });
    $.each(jQuery('#frmFirma input[name=imageFile]')[0].files, function (i, file) {
        dataFoto.append('firma', file);
    });
    dataFoto.append('id', $('#idFoto').val());

    $.ajax({
        url: urlBlob,
        data: dataFoto,
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
        type: 'POST', // For jQuery < 1.9
        success: function (datoF) {
            alert("Informacion guardada correctamente");
            $.ajax({
                url: urlBlob + "/" + $('#idFoto').val(),
                method: 'GET',
                type: 'GET', // For jQuery < 1.9
                success: function (persona) {
                    //alert("datos recuperados correctamente");
                    console.log(persona);
                    var base64Foto = persona.foto;
                    var base64Firma = persona.firma;
                    document.getElementById('imgFoto').setAttribute(
                        'src', 'data:image/jpeg;base64,' + base64Foto);
                    document.getElementById('imgFirma').setAttribute(
                        'src', 'data:image/jpeg;base64,' + base64Firma);

                }
            });
        }
    });
});

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};


$(document).ready(function () {
    var urlBlob = contexto + "personas/blob";
    var id = getUrlParameter('id');    

    if (undefined !== id) {
        
        $.ajax({
            url: urlBlob + "/" + id,
            method: 'GET',
            type: 'GET', // For jQuery < 1.9
            success: function (persona) {
                //alert("datos recuperados correctamente");
                console.log(persona);
                $("#idPersona").val(id);
                $("#nombre").val(persona.nombre);
                $("#apellidos").val(persona.apellidos);
                $("#rfc").val(persona.rfc);
                $("#direccion").val(persona.direccion);
                $("#apellidos").val(persona.apellidos);
                if (persona.permisoMatar == 1) {
                    $('#permiso').prop('checked', true);
                }                
                var base64Foto = persona.foto;
                var base64Firma = persona.firma;
                document.getElementById('imgFoto').setAttribute(
                    'src', 'data:image/jpeg;base64,' + base64Foto);
                document.getElementById('imgFirma').setAttribute(
                    'src', 'data:image/jpeg;base64,' + base64Firma);

            }
        });
    }

});
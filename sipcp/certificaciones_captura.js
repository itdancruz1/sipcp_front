// this is the id of the form
$("#frmCert").on("submit", function (e) {
    var url = contexto + "certificaciones";
    e.preventDefault(); // avoid to execute the actual submit of the form.

    var datos = $('#frmCert').serializeJSON();
    datos = JSON.stringify(datos);
    console.log(datos);

    $.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
        contentType: 'application/json',
        mimeType: 'application/json',
        data: datos, // serializes the form's elements.
        success: function (data) {
            console.log(JSON.stringify(data)); // show response from the php script.
            alert("Datos guardados correctamente");

        }
    });

});


var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};


$(document).ready(function () {
    var urlBlob = contexto + "personas/blob";
    var id = getUrlParameter('id');

    if (undefined !== id) {

        $.ajax({
            url: urlBlob + "/" + id,
            method: 'GET',
            type: 'GET', // For jQuery < 1.9
            success: function (persona) {
                //alert("datos recuperados correctamente");
                console.log(persona);
                $("#idPersona").val(id);
                $("#nombre").val(persona.nombre);
                $("#apellidos").val(persona.apellidos);
                $("#rfc").val(persona.rfc);
                $("#direccion").val(persona.direccion);
                $("#apellidos").val(persona.apellidos);
                if (persona.permisoMatar == 1) {
                    $('#permiso').prop('checked', true);
                }
                var base64Foto = persona.foto;
                var base64Firma = persona.firma;
                document.getElementById('imgFoto').setAttribute(
                    'src', 'data:image/jpeg;base64,' + base64Foto);
                document.getElementById('imgFirma').setAttribute(
                    'src', 'data:image/jpeg;base64,' + base64Firma);

            }
        });
    }


    var urlcom = contexto + "patentes?filtro="

    $("#patente").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: urlcom + request.term,
                dataType: "json",
                data: {
                    q: request.term
                },
                success: function (data) {
                    response($.map(data, function (item) {
                        var AC = new Object();

                        //autocomplete default values REQUIRED
                        AC.label = item.numPatente;
                        AC.value = item.numPatente;

                        //extend values
                        AC.idPatente = item.idPatente;
                        return AC

                    }));
                }
            });
        },
        minLength: 3,
        select: function (event, ui) {
            console.log(JSON.stringify(ui))
            $("#idPatente").val(ui.item.idPatente);
        },
        open: function () {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function () {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    });


});


$(function () {


    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('yyyy/mm/dd', { 'placeholder': 'yyyy/mm/dd' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#fechahasta').datetimepicker({
        format: 'YYYY/MM/DD'
    });
    $('#fechaEmision').datetimepicker({
        format: 'YYYY/MM/DD'
    });
      
    //Timepicker
    $('#timepicker').datetimepicker({
        format: 'LT'
    })

})
  // BS-Stepper Init
$(function () {

  var urlPatente = contexto + "patentes"
  $("#frmPatente").on("submit", function (e) {

    e.preventDefault(); // avoid to execute the actual submit of the form.   
    var datos = $('#frmPatente').serializeJSON();

    datos = JSON.stringify(datos);

    $.ajax({
      type: "POST",
      url: urlPatente,
      dataType: 'json',
      contentType: 'application/json',
      mimeType: 'application/json',
      data: datos, // serializes the form's elements.
      success: function (data) {
        console.log(JSON.stringify(data)); // show response from the php script.
        console.log("ID " + data.respuesta.idPatente);

        var urlBlob = contexto + "patentes/blob";

        var dataFoto = new FormData();
        $.each(jQuery('#frmPatente input[name=imageFile]')[0].files, function (i, file) {
          dataFoto.append('fierro', file);
        });

        dataFoto.append('id', data.respuesta.idPatente);

        $.ajax({
          url: urlBlob,
          data: dataFoto,
          cache: false,
          contentType: false,
          processData: false,
          method: 'POST',
          type: 'POST', // For jQuery < 1.9
          success: function (datoF) {
            alert("Informacion guardada correctamente");
            $.ajax({
              url: urlBlob + "/" + data.respuesta.idPatente,
              method: 'GET',
              type: 'GET', // For jQuery < 1.9
              success: function (patente) {
                //alert("datos recuperados correctamente");
                console.log(patente);
                var base64Fierro = patente.fierro;
                document.getElementById('imgFierro').setAttribute(
                  'src', 'data:image/jpeg;base64,' + base64Fierro);

              }
            });
          }
        });



      }
    });


  });

  var url = contexto + "personas?filtro="

  $("#nombre").autocomplete({
    source: function (request, response) {
      $.ajax({
        url: url + request.term,
        dataType: "json",
        data: {
          q: request.term
        },
        success: function (data) {
          response($.map(data, function (item) {
            var AC = new Object();

            //autocomplete default values REQUIRED
            AC.label = item.nombre + " " + item.apellidos + " [" + item.rfc + "]";
            AC.value = item.nombre + " " + item.apellidos;

            //extend values
            AC.rfc = item.rfc;
            AC.direccion = item.direccion;
            AC.permisoMatar = item.permisoMatar;
            AC.idPersona = item.idPersona;

            return AC

          }));
        }
      });
    },
    minLength: 3,
    select: function (event, ui) {
      console.log(JSON.stringify(ui))
      $("#rfc").val(ui.item.rfc);
      $("#idPersona").val(ui.item.idPersona);
      $("#direccion").val(ui.item.direccion);
      if (ui.item.permisoMatar == 1) {
        $('#permiso').prop('checked', true);
      }
    },
    open: function () {
      $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
    },
    close: function () {
      $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
    }
  });

  var urlPredio = contexto + "predios?filtro="

  $("#predio").autocomplete({
    source: function (request, response) {
      $.ajax({
        url: urlPredio + request.term,
        dataType: "json",
        data: {
          q: request.term
        },
        success: function (data) {
          response($.map(data, function (item) {
            var AC = new Object();

            //autocomplete default values REQUIRED
            AC.label = item.predio;
            AC.value = item.predio;

            //extend values
            AC.direccion = item.direccion;
            AC.idPredio = item.idPredio;

            return AC

          }));
        }
      });
    },
    minLength: 3,
    select: function (event, ui) {
      console.log(JSON.stringify(ui))
      $("#idPredio").val(ui.item.idPredio);
      $("#ubicacion").val(ui.item.direccion);
    },
    open: function () {
      $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
    },
    close: function () {
      $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
    }
  });

});





$(function () {


  //Datemask dd/mm/yyyy
  $('#datemask').inputmask('yyyy/mm/dd', { 'placeholder': 'yyyy/mm/dd' })
  //Datemask2 mm/dd/yyyy
  $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
  //Money Euro
  $('[data-mask]').inputmask()

  //Date range picker
  $('#fechade').datetimepicker({
    format: 'YYYY/MM/DD'
  });
  $('#fechahasta').datetimepicker({
    format: 'YYYY/MM/DD'
  });
  //Date range picker
  $('#reservation').daterangepicker()
  //Date range picker with time picker
  $('#reservationtime').daterangepicker({
    timePicker: true,
    timePickerIncrement: 30,
    locale: {
      format: 'MM/DD/YYYY hh:mm A'
    }
  })
  //Date range as a button
  $('#daterange-btn').daterangepicker(
    {
      ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      startDate: moment().subtract(29, 'days'),
      endDate: moment()
    },
    function (start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
    }
  )

  //Timepicker
  $('#timepicker').datetimepicker({
    format: 'LT'
  })

})
// BS-Stepper Init
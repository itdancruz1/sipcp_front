$(document).ready(function () {
    var url = contexto + "predios";
    $.ajax({

        url: url,
        method: 'GET',
        dataType: 'json',
        success: function (data) {        

            $('#tblpredios').DataTable({
                "paging": true,
                "lengthChange": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true, "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
                data: data,
                columns: [

                    { "data": "idPredio" },
                    { "data": "predio" },
                    { "data": "direccion" },
                    { "data": "idPredio",
                    render: function ( dato, type, row ) {
                        return '<a href="predios_captura.html?id='+ dato + '">Revisar </a>';
                    }}
                ]
            }).buttons().container().appendTo('#tblpredios_wrapper .col-md-6:eq(0)');
        }
    });


});